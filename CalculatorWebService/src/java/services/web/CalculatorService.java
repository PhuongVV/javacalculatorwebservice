/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services.web;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
/**
 *
 * @author Bi
 */
@WebService(serviceName = "calculator")
@Stateless()
public class CalculatorService {

    
    
    @WebMethod(operationName = "add")
    public double add(@WebParam(name = "i") double i, @WebParam(name = "j") double j) {
        return i+j;
    }
    
    
    @WebMethod(operationName = "subtract")
    public double subtract(@WebParam(name = "i") double i, @WebParam(name = "j") double j) {
        return i-j;
    }
    
    
    @WebMethod(operationName = "multiply")
    public double multiply(@WebParam(name = "i") double i, @WebParam(name = "j") double j) {
        return i*j;
    }
    
    
    @WebMethod(operationName = "divide")
    public double divide(@WebParam(name = "i") double i, @WebParam(name = "j") double j) {
        return i/j;
    }
    
}
